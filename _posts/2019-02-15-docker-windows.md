---
layout: post
title: Switching to Docker from Vagrant
categories: [development environment]
tags: [docker, windows, development]
description: The shear joy of using Docker as opposed to Vagrant
comments: true
---

## Getting by with Vagrant

A little over year ago, I was invited to join the team over at [Ishtar-Collective]('https://www.ishtar-collective.net'). All my previous web development experience was in the land of either PHP or JavaScript. So, you can imagine my surprise when I learned that Ishtar was a Rails based site. Can we just say that I had a very mild to serious panic attack.

In the fertile safe lands of PHP, I had grown quite accustomed to using Vagrant for my environment workflows. It was easy to configure and was quite performant to be honest. And although I could configure my own custom Vagrant environments, Laravel's Homestead project was a God send. This is what I used most of the time when spinning up a site for an idea or just messing around with some simple PHP or JavaScript command line experiments.

> Yes! I am a web developer and work on Windows!

After joining the Ishtar team and speaking directly with Baxter about setting up the project, we quickly learned that Ishtar just would not work in Windows. There was a single Gem being used in the project that keep me from running the site in a Windows environment and that was [rbenv]('https://github.com/rbenv/rbenv). Now, not knocking the project, but there is no support for Windows and there will not be support anytime soon. I figured since I was working on a new project and was about to dive into a host of new technology, I might as well try something new and setup [WSL]('https://docs.microsoft.com/en-us/windows/wsl/install-win10'). Yeah... that didn't work well at all. At this time, it might be perfectly fine, but a year ago it was rough. Vagrant to the rescue!

It took me all of a few hours to spin up a custom Vagrant environment that allowed me to get to work ASAP. You can view the script in the following [Gist](https://gist.github.com/unisys12/6819ef48ba095dc2bbaf3f24206cb972). Bear in mind that I've done a poor job of keeping this up to date. We've since added ElasticSearch and this is not documented here. I know, I know... Shame on me!

It was a solid year of working on the project using Vagrant and I would still suggest it to anyone. It's a great tool and the support is awesome. And I will be honest, there's something about having what I call _"A real box to work with"_ that makes it so satisfying. Honestly, I still feel it's as close to a _real_ production environment as you can get.

## Why Docker?

So why move from Vagrant to Docker? Well, it's quite simple. The team does not use Vagrant and does not prefer it as their environment of choice. They hated that I had to go through the troubles that I did, just so "I" could work on the project, but admired that I was willing to "do the work to make it happen". :) We'll call it brownie points and call it even. So what where they using if they didn't like Vagrant and didn't currently have a Docker solution? I could only guess.. nothing.

Early on, I created a branch of the ishtar project and created a base environment for Docker, but I literally could not run it on my current dev laptop. After a reboot, it took almost 5 mins for Docker itself to come to ready. I mean it was absurd! After opening a instance of Chrome, Cmder, VSCode and launching the site.... I couldn't code. We couldn't even consider opening separate tabs for things like research. It was just nasty. I don't think all this has to do with the Docker Client for Windows. I mean, most of the issues started after turning on WSL. So Hypervisor has some blame here as well.

It has been a long-term goal of mine to consolidate our teams dev environments for sometime now, as explained above. Over the last few months, my environment has gotten slower and slower. Slower being defined as, rendering our sites homepage taking 15 seconds within my Vagrant environment. With the recent drop in SSD prices, I recently purchased a 250GB WD Blue SSD for less than \$75 dollars. After getting it and Windows 10 installed, I quickly installed Docker. Then pulled down my working branch of my `docker-support` branch. I was astounded at how quickly I was troubleshooting my working environment and not recourses. Sure, Vagrant might perform the same, but there was no 1+ gig download of a single Linux image. That, for me, is a big deal!

At the time of this post, I'm still perfecting the Ishtar Docker environment, but I can honestly say - "Why did I wait so long to experience this?"\_ Docker config seems to be so much easier and flexible than Vagrant. Honestly, if I had to compare Docker and Vagrant within the realm of frontend development - I would compare it to having Babel today, compared to AMD or RequireJS. Or Sass before Sass was thing. I mean... Really?

Resource wise, Docker is way lower. I have no idea what changes where made between the earlier Windows 10 builds and 1809, but there is a hella big difference. Granted, there are still some _issues_ to work through, but it's totally worth it.
