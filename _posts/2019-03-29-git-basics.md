---
layout: post
title: GIT Basics
categories: [development, teams]
tags: [git, development]
description: Basic Git workflow when working in a team
comments: true
---

## GIT Magic

For so many developers, GIT is the ultimate black magic. You can master your framework or even language of choice. But GIT... well, that's a different story. Don't get me wrong. I realize there are tons of developers out there that understand GIT like it's a second language. But, there are many of us that do not. This post... This post is for them.

## Basics

GIT basics are sort of a given here, but for those that don't know - there's a few cmds you need to understand and they are as follows:

### Git Clone

```bash
git clone <git_repository_path/>
```

The above cmd will clone the passed repository path to a dir relative to your current directory. So! If you are within your `C:/` dir and you clone a repository named `BADA$$_REPO`, it will create a new directory called `BADA$$_REPO` under `C:/` which can be accessed by `C:/BADA$$REPO`. You can then `cd` into that dir and start working.

```bash
cd BADA$$REPO
```

### Git Fork

This forks another project into your own Git Repo within your account. When contributing to other repo's, it's a best practice to fork their project into your own and alter it from there. This allows you to isolate your changes from everyone else. An example of this would look like the following:

```bash
git fork BADA$$_REPO
```

To be completely honest - You don't need to worry a lot about this cmd. Most of the time, your going to do this from within the Git, Bitbucket or GitLab websites. Typically, there will be a Fork button on the page that you will select and boom! Consider life ended.
From there, you just clone down your forked version of the repo and make commits. Then submit pull requests from your repo.

### Git Checkout -b <feature_branch_name>

So! A best practice within any versioned based workflow is to create a new branch off of the master branch. In turn, this creates your feature branch that you will make your changes on. Now, this might change slightly from project to project, but you can bet on this method most of the time. So, what do I mean by `feature_branch_*`?

Well, this is core to Git understanding and that is a tree. Honestly, just think of a tree. Go outside and look at any tree and look at where the base of that tree meets the ground. That would represent the master branch of a repo. Then look up the trunk of that tree and you quickly see a series of branches off this main root. These branches represent a single upstream or pending change to this repo. These branches are represented as branches. That is what `-b` stands for for, `branch`. `git checkout -b <new feature/>`.

### Git Commit

Commits. One of the most divisive cmds with the Git ecosystem. Some say to commit often and others say don't. What does this mean? Why does it matter? Well, first of all - it depends on what the repo maintainer has asked of you. Most will ask you to keep your pull requests as small as possible. That does not mean keep your code changes as small as possible, that means keep your pull requests as small as possible. What the hell does that mean? Yeah, I know. So let's look at an example.

Example of Best Practice Scenario:

- You find a bug within a repo or want to resolve a current open issue.
- You make the changes to all the files needed to resolve the issue.
- You commit those changes and submit a pull request to resolve those changes.

Example of Non-Best Practice Scenario:

- You find a bug within a repo or want to resolve a current open issue.
- You make the changes to all the files needed to resolve the issue.
- You commit those changes.
- You see that some files are not following ES6 _(JavaScipt)_ function declaration best practices, so you make changes to 5 other files and commit those changes as well.
- Then you submit your pull request.

Either way, you _add_ all the files changed in to resolve the issue or fix the bug and a best practice for commits is to include a message that relect's the changes that you made to the file:

```bash
git add <file changed/>
git commit -m "this is what I changed!"
```

### Pull Requests

Yeah, these just sorta happen like I explained above. But, they happen from within the website.

> "What in the hell do you mean by that?"

What I mean is, they happen within the site from which the repo originates. Is the repo on Github? GitLab? BitBucket? It doesn't matter. After you commit and push up the changes to your fork - once you visit your repo, you will see a visual aid at the top of your page that signals you to submit a merge/pull request. You click on this and fill in your information to the form.

This is where most people get stumped, to be honest. I did, on my first pull request. But there is nothing for you to do on the cmd line really. I mean, it might be, but... everyone else just goes to their repo and click on the "submit pull request` button.

## Summary

Honestly, this is where you just have to dive in and give it a go to see what happens. I cannot possibly outline all the different possible outcomes. But be assured, you are not alone. There are far more repo owners out there that are willing to help you through your final steps than there are people that are straight up ass holes.

If it had not been for my first experience, I might never had contributed back through Git. But because it was a solid positive experience, I ended up making over 13 commits to the docs of [PhalconPHP](https://github.com/phalcon/docs/pulls?q=is%3Apr+author%3Aunisys12+is%3Aclosed). Not to mention all the other projects I did for myself and with others. Next time, I will cover a slightly more advanced topic of Git workflows.
