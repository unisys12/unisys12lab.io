---
layout: post
title: Initial Commit
categories: [general]
tags: [purpose, goals]
description: Post to outline purpose and goals of this blog.
comments: true
---

## Purpose

Given that web development is only a hobby for me, I think I get to work on some pretty cool projects from time to time and I want to share those experiences with others. I also would like to use this blog as a place for me to document me learning new things or playing with new languages.

## Cadence

There probably will not be a cadence to when new posts are deployed, in the beginning. I would like to get at least one post up a week, but if I'm being honest with you and myself - I really don't know if I'll have time to do that consistently enough. So, I'm not gonna make that promise. For now, when a topic or project strikes me as _"worthy"_ I will take the time to write something up.

## Upcoming Projects

I know of three projects that I will be tackling over the coming weeks or months.

- Ishtar Blog
- Mobile Menu for Ishtar
- Create my own theme for this blog
- Start work on a Destiny related project unrelated to Ishtar _(might get to this one)_

I am currently using [Jekyll](https://jekyllrb.com/) for this iteration of the blog. So far, I'm liking it. I am using a really outdated Jekyll theme by the name of [Dbyll](https://github.com/dbtek/dbyll). Being totally honest, I didn't want to use a theme. I wanted to create my own. But I also wanted the blog up and running, so... theme it was. What I would like to do is learn the process of creating a pattern library or design system for the blog. I can them carry this knowledge over to Ishtar. Issue here is that Ishtar's needs are greater than mine. So I might have to do the learning there.

As for the Ishtar blog, Baxter and I are not happy with the current solutions that we've looked at for blogging systems. One solution that I've suggested is using a Headless CMS combined with a Static Site Generator. This would give use some of the flexibilities we crave, but at the cost of complexity and cost. As with any other project, cost is always a concern at Ishtar. We have an amazing group of [Patrons](https://www.patreon.com/ishtarcollective) and we want to be good stewards of their donations. A much simpler solution would be to write out our own Headless CMS/API and put our own front end on it. Then again... _walks away_

As I get started on these projects, I will most likely add a **Projects** collection here, which will make organizing the posts a lot easier. So yeah! It's going to be a busy year already. It's most likely gonna be a bit bumpy as well.

Until next time.
