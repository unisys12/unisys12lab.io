---
layout: post
title: Container Failure on Windows 10 1809
categories: [environment]
tags: [docker, windows, development]
description: Making Docker work with Windows 10 1809
comments: true
---

## Install is no Problem

Installing Docker for Windows is fine. No worries. But making it work was actually hell for me. So let me share my experience in hopes that it will make it easier for others later on.

## vmcompute.exe

There was a change in Hyper-V, apparently in Build 1809, that makes running any type of containers impossible. To correct this, follow these steps.

1. Open "Window Security"
2. Open "App & Browser control"
3. Click "Exploit protection settings" at the bottom
4. Switch to "Program settings" tab
5. Locate "C:\WINDOWS\System32\vmcompute.exe" in the list and expand it
6. Click "Edit"
7. Scroll down to "Code flow guard (CFG)" and uncheck "Override system settings"
8. Start vmcompute from powershell "net start vmcompute"

After an hour of rummaging through my system, trying to diagnose the problem, I finally started running some Google searches. A few lead me to a Github Issue Tread that in turn pointed to a [Windows 10 Virtualization](https://social.technet.microsoft.com/Forums/en-US/ee5b1d6b-09e2-49f3-a52c-820aafc316f9/hyperv-doesnt-work-after-upgrade-to-windows-10-1809?forum=win10itprovirt) thread that had to solution above.
